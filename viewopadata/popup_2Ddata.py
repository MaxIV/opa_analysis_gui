import numpy as np
from PyQt5.QtWidgets import (
    QWidget,
    QMainWindow,
    QGridLayout,
    QApplication,
)
import sys
import pyqtgraph as pg

from pyqtgraph.Qt import QtGui

try:
    from viewopadata.utils import load_analysis_data
except Exception:
    from utils import load_analysis_data
from dateutil import parser

pg.setConfigOptions(imageAxisOrder="row-major")


class CustomRoi(pg.ROI):
    def __init__(
        self,
        pos,
        size=...,
        angle=0,
        invertible=False,
        maxBounds=None,
        snapSize=1,
        scaleSnap=False,
        translateSnap=False,
        rotateSnap=False,
        parent=None,
        pen=None,
        hoverPen=None,
        handlePen=None,
        handleHoverPen=None,
        movable=True,
        rotatable=True,
        resizable=True,
        removable=False,
        aspectLocked=False,
    ):
        super().__init__(
            pos,
            size,
            angle,
            invertible,
            maxBounds,
            snapSize,
            scaleSnap,
            translateSnap,
            rotateSnap,
            parent,
            pen,
            hoverPen,
            handlePen,
            handleHoverPen,
            movable,
            rotatable,
            resizable,
            removable,
            aspectLocked,
        )
        self.sigRegionChanged.connect(self.update_plot)
        self.addScaleHandle([1, 1], [0, 0])

    def update_plot(self):
        self.getState()


class customImageview(pg.ImageView):
    def __init__(
        self,
        parent=None,
        name="ImageView",
        view=None,
        imageItem=None,
        levelMode="mono",
        discreteTimeLine=False,
        roi=None,
        normRoi=None,
        *args
    ):
        super().__init__(
            parent,
            name,
            view,
            imageItem,
            levelMode,
            discreteTimeLine,
            roi,
            normRoi,
            *args
        )

    def roiChanged(self):
        # Extract image data from ROI
        if self.image is None:
            return

        image = self.getProcessedImage()

        # getArrayRegion axes should be (x, y) of data array for col-major,
        # (y, x) for row-major
        # can't just transpose input because ROI is axisOrder aware
        colmaj = self.imageItem.axisOrder == "col-major"
        if colmaj:
            axes = (self.axes["x"], self.axes["y"])
        else:
            axes = (self.axes["y"], self.axes["x"])

        data, coords = self.roi.getArrayRegion(
            image.view(np.ndarray),
            img=self.imageItem,
            axes=axes,
            returnMappedCoords=True,
        )

        if data is None:
            return
        # print(data.shape)
        scalex = self.imageItem.transform().m11()
        # Convert extracted data into 1D plot data
        if self.axes["t"] is None:
            # Average across y-axis of ROI
            data = data.mean(axis=self.axes["y"])

            # get coordinates along x axis of ROI mapped to range (0, roiwidth)
            if colmaj:
                start = coords[1, 0, 0:1]
                start0 = coords[0, 0:1, 0]
                # print(start)
                coords = coords[:, :, 0] - coords[:, 0:1, 0]

            else:
                start = coords[1, 0:1, 0]
                start0 = coords[0, 0, 0:1]
                # print(start)
                coords = coords[:, 0, :] - coords[:, 0, 0:1]
            # print(start)
            xvals = (
                ((coords**2).sum(axis=0) ** 0.5) * scalex
                + start * scalex
                + start0
            )
        else:
            # Average data within entire ROI for each frame
            data = data.mean(axis=axes)
            xvals = self.tVals

        # Handle multi-channel data
        if data.ndim == 1:
            plots = [(xvals, data, "w")]
        if data.ndim == 2:
            if data.shape[1] == 1:
                colors = "w"
            else:
                colors = "rgbw"
            plots = []
            for i in range(data.shape[1]):
                d = data[:, i]
                plots.append((xvals, d, colors[i]))

        # Update plot line(s)
        while len(plots) < len(self.roiCurves):
            c = self.roiCurves.pop()
            c.scene().removeItem(c)
        while len(plots) > len(self.roiCurves):
            self.roiCurves.append(self.ui.roiPlot.plot())
        for i in range(len(plots)):
            x, y, p = plots[i]
            self.roiCurves[i].setData(x, y, pen=p)


class PopUpWindow(QMainWindow):
    def __init__(self, file_prefix=""):
        super().__init__()
        # setting title
        self.setWindowTitle("Compiled 2D image")
        self.file_prefix = file_prefix
        # setting geometry
        self.setGeometry(100, 100, 600, 500)
        self.bool_good_data = self.compile2Ddata()
        if self.bool_good_data:
            self.UiComponents()

        self.show()

    # method for components

    def compile2Ddata(self):
        # file_prefix ='/Users/mihpop/Git_repos/hippie/opa_data_mihai_test/Analysis_results/2022-11-24_test_hippie.0_analysed.h5'
        # folder = os.path.dirname(self.file_prefix)
        # folder = os.path.basename(file_prefix)
        # file_prefix1 = os.path.basename(self.file_prefix).split(".")[0]
        brute_data = []
        x_data = []
        y_data = []
        for full_path_file in self.file_prefix:
            # for root, dirs, files in os.walk(folder):
            #     for file in files:
            #         if file.startswith(file_prefix1):
            #             full_path_file = os.path.join(folder, file)
            # with h5py.File(full_path_file,'r') as f:
            #     print(f.keys())
            x, y, y_fit, t = load_analysis_data(file=full_path_file)
            brute_data.append(y - y_fit)
            y_data.append(parser.parse(t))
            # print(len(x))

            if len(x_data) < len(x):
                x_data = x
        # self.x=x
        self.x_data = np.array(x_data)
        try:
            self.data2D = np.fliplr(np.array(brute_data))
            return True
        except Exception:

            return False

    def UiComponents(self):

        # creating a widget object
        widget = QWidget()

        # setting configuration options
        pg.setConfigOptions(antialias=True)
        plot = pg.PlotItem()
        plot.setLabel(axis="left", text="Y-axis")
        plot.setLabel(axis="bottom", text="Energy [eV]")

        # plot.addItem(roi)
        # creating image view object
        self.imv = customImageview(view=plot)
        # allow zooming in both directions
        self.imv.view.setAspectLocked(False)

        # self.imv.roi.setSize([self.x_data[-1]-self.x_data[0], self.data2D.shape[0]])
        # remove ticks from histogram
        self.imv.ui.histogram.axis.setTicks([])

        self.imv.setImage(self.data2D, xvals=self.x_data)
        # self.imv.setImage(self.data2D,xvals=np.linspace(0.,2.,len(self.x_data)))

        # Set a custom color map
        colors = [
            (0, 0, 0),
            (45, 5, 61),
            (84, 42, 55),
            (150, 87, 60),
            (208, 171, 141),
            (255, 255, 255),
        ]

        # color map
        cmap = pg.ColorMap(pos=np.linspace(0.0, 1.0, 6), color=colors)

        # setting color map to the image view
        self.imv.setColorMap(cmap)
        self.imv.ui.histogram.setVisible(False)
        self.imv.ui.roiBtn.setVisible(False)
        self.imv.ui.menuBtn.setVisible(False)

        # Creating a grid layout
        layout = QGridLayout()

        # setting this layout to the widget
        widget.setLayout(layout)

        # plot window goes on right side, spanning 3 rows
        layout.addWidget(self.imv, 0, 0, 3, 1)

        # setting this widget as central widget of the main window
        self.setCentralWidget(widget)
        # self.imv.adjustSize()

        tr = QtGui.QTransform()
        dX = self.x_data[0] - self.x_data[-1]
        self.imv.imageItem.setTransform(
            tr.translate(self.x_data[-1], 0).scale(
                dX / self.x_data.shape[0], 1
            )
        )
        # print(dX, self.x_data.shape[0])
        # self.imv.imageItem.setTransform(tr.scale(1, 1).translate(self.x_data[-1], 0))
        # self.imv.imageItem.setRect(
        #     QRectF(
        #         self.x_data.min(), 0, self.x_data.max(), self.data2D.shape[0]
        #     )
        # )
        # roi = CustomRoi(
        #     pos=[self.x_data[-1], 0],
        #     size=[abs(self.x_data[-1] - self.x_data[0]), self.data2D.shape[0]],
        #     resizable=True,

        # )

        # self.imv.addItem(roi)
        self.imv.ui.roiBtn.setChecked(True)
        self.imv.roiClicked()

        # self.imv.roi.sigRegionChangeFinished.connect(lambda: update_roiplot_xdata(self.imv.roi))
        self.imv.roi.setPos((self.x_data[-1], 0))
        self.imv.roi.setSize(
            (self.x_data[0] - self.x_data[-1], self.data2D.shape[0])
        )
        self.imv.roi.rotatable = False
        # print(self.x_data[0] - self.x_data[-1])

        self.imv.autoRange()


# Connect the callback to the ROI's sigRegionChangeFinished signal


# Driver Code
if __name__ == "__main__":
    # create pyqt5 app
    App = QApplication(sys.argv)
    file_prefix = [
        "/Users/mihpop/Git_repos/hippie/opa_data_mihai_test/Analysis_results/2022-11-23_enclosure_test.0_analysed.h5",
        "/Users/mihpop/Git_repos/hippie/opa_data_mihai_test/Analysis_results/2022-11-23_enclosure_test.1_analysed.h5",
        "/Users/mihpop/Git_repos/hippie/opa_data_mihai_test/Analysis_results/2022-11-23_enclosure_test.2_analysed.h5",
    ]
    # create the instance of our Window
    window = PopUpWindow(file_prefix=file_prefix)

    # start the app
    sys.exit(App.exec_())
