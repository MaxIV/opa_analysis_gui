import numpy as np
from scipy.special import jv
from lmfit import Model


def FunctionFit(model, params, ydata, xdata):
    function_fits = []
    if len(ydata.shape) > 1:
        for i in range(ydata.shape[0]):
            result = model.fit(ydata[i, :], params, x=xdata)
            function_fits.append(result)
    else:
        result = model.fit(ydata[:], params, x=xdata)
        function_fits.append(result)
    return function_fits


def PolyFunctionFits(npoly, ydata, xdata):
    function_fits = []
    if len(ydata.shape) > 1:
        for i in range(ydata.shape[0]):
            p = np.polyfit(x=xdata, y=ydata[i, :], deg=npoly)
            y_f = np.poly1d(p)
            result = y_f(xdata)
            function_fits.append(result)
    else:
        p = np.polyfit(x=xdata, y=ydata[:], deg=npoly)
        y_f = np.poly1d(p)
        result = y_f(xdata)
        function_fits.append(result)
    return function_fits


# Custom Functions
# https://lmfit.github.io/lmfit-py/model.html#model-class-methods
def besselj0(x, amp0, nu00):
    y = amp0 * np.abs(jv(0, np.pi / nu00 * x))
    return y


def besselj2(x, f1, f2, nu02, OFF):
    # x is the wavenumber nu
    # amp is a scaling factor of the amplitude
    # phi 0 is just pi/nu0
    y = (f1 * (x / nu02) + f2 * (x**2 / nu02)) * np.abs(
        jv(2, (np.pi / nu02) * x)
    ) + OFF
    return y


def fit_bessel(X_cut, Y_cut):
    bmodel = Model(besselj2)
    paramsBes2 = bmodel.make_params()
    paramsBes2["f1"].set(value=1.0, min=0.0, max=4)
    paramsBes2["f2"].set(value=7.5e-5, min=-1, max=1)
    paramsBes2["nu02"].set(value=2140, min=2100, max=2700)
    paramsBes2["OFF"].set(value=0.1, min=0, max=0.5)

    result_fits = FunctionFit(bmodel, paramsBes2, Y_cut, X_cut)

    # Y_fit = bmodel.eval(result_fits[0].params,X_cut)

    return result_fits


if __name__ == "__main__":
    print("no no\n")
