import os
from brukeropusreader import read_file
import numpy as np
import bisect
import h5py
from dateutil import parser


def isint(s=""):
    try:

        int(s)
        flag = True

    except ValueError:

        flag = False
    return flag


def get_scans(folder=""):
    prefixes = {}
    for file in os.listdir(folder):
        ret = file.rsplit(".", 1)
        if len(ret) > 1:

            if isint(ret[1]):
                pref = ret[0]
                ind = int(ret[1])
                if pref not in list(prefixes.keys()):
                    prefixes[pref] = [ind]
                else:
                    bisect.insort(prefixes[pref], ind)
    return prefixes


def get_analysed_scans(folder=""):
    prefixes = {}
    for file in os.listdir(folder):
        ret = file.rsplit(".", 3)

        if len(ret) > 1 and "analysed" in ret[1]:

            if isint(ret[1].split("_")[0]):
                pref = ret[0]
                ind = int(ret[1].split("_")[0])
                if pref not in list(prefixes.keys()):
                    prefixes[pref] = [ind]
                else:
                    bisect.insort(prefixes[pref], ind)
    return prefixes


def load_analysis_data(file=""):
    with h5py.File(file, "r") as f:
        x = f["cut_x"][:]
        y = f["cut_y"][:]
        y_fit = f["fit_y"][:]
        t = f["time"][()]
    return x, y, y_fit, t


def load_data(file=""):
    Z3 = read_file(
        file
    )  # returns a dictionary of the data and metadata extracted
    start = Z3["AB Data Parameter"]["FXV"]
    npts = Z3["AB Data Parameter"]["NPT"]
    stop = Z3["AB Data Parameter"]["LXV"]

    X = np.linspace(start, stop, npts)
    Y = Z3["AB"]
    time = Z3["IgRf Data Parameter"]["TIM"]
    date = Z3["IgRf Data Parameter"]["DAT"]
    timenoew = date + " " + time.split(" ")[0]
    new_time = parser.parse(timenoew)
    time_str = new_time.strftime("%Y-%m-%d %H:%M")

    del Z3, timenoew, date, new_time
    return (X, Y, time_str)


if __name__ == "__main__":
    folder = "/Users/mihpop/Git_repos/hippie/opa_data_mihai_test/Lisa_PMIRRAS"
    print(get_scans(folder=folder))
