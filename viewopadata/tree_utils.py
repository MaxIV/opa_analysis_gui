from PyQt5 import QtWidgets as QW
from PyQt5 import QtCore, QtGui
import pyqtgraph as pg
from PyQt5.QtGui import QStandardItemModel, QStandardItem

from viewopadata.utils import (
    get_scans,
    get_analysed_scans,
    load_data,
    load_analysis_data,
)
import random


class ColorColumn(QtGui.QStandardItem):
    """
    The column item that can be clicked to change color of the plot
    """

    def __init__(self, scan=None, color=None):
        QtGui.QStandardItem.__init__(self)
        self.color = color
        self.setEditable(False)
        self.setSelectable(False)
        self.scan = scan

    def setColor(self, color):
        brush = QtGui.QBrush(color)
        self.setBackground(brush)


class ScanModel(QtGui.QStandardItemModel):
    """
    ScanModel interfaces from treeview with fileitems and scanitems
    """

    def __init__(self, parent=None):
        QtGui.QStandardItemModel.__init__(self, 0, 1)
        self.clear()

    def clear(self):
        """
        Clear the scanmodel, set the columns and clear the filelist
        """
        QtGui.QStandardItemModel.clear(self)
        self.setColumnCount(2)
        self.setHeaderData(0, QtCore.Qt.Horizontal, QtCore.QVariant("Scan"))
        self.setHeaderData(1, QtCore.Qt.Horizontal, QtCore.QVariant("Color"))

    def appendRow(self, item):
        if hasattr(item, "color"):
            QtGui.QStandardItemModel.appendRow(self, [item, item.colorColumn])
        else:
            QtGui.QStandardItemModel.appendRow(self, [item])


class fileItem(QStandardItem):
    def __init__(self) -> None:
        super().__init__()
        self.file = ""
        self.loaded = False

    def loadData(self):
        X, Y, T = load_data(file=self.file)
        self.X_raw = X
        self.Y_raw = Y
        self.T = T


class analysis_item(fileItem):
    def __init__(self) -> None:
        super().__init__()
        self.plotDataItem = pg.PlotDataItem()
        self.isDisplayed = False
        self.colorColumn = ColorColumn(self)
        self.randomColor()

    def loadData(self):
        X, Y, Y_fit, t = load_analysis_data(self.file)
        self.X = X
        self.Y = Y
        self.Y_fit = Y_fit
        self.T = t
        self.plotDataItem.setData(X, Y - Y_fit)

    def setColor(self, color):
        """
        Set the color of the plots. Sets the color of the colorcolumn in
         the treeview. Inherit to also set on plots,
        """
        self.colorColumn.setColor(color)
        pen = pg.mkPen(color)
        brush = pg.mkBrush(color)
        self.plotDataItem.setPen(pen)
        self.plotDataItem.setSymbolBrush(brush)

    def randomColor(self):
        """
        Sets the color to a random color
        """
        color = pg.intColor(random.randint(1, 20))
        self.setColor(color)


class Local_treeView(QW.QTreeView):
    def __init__(self) -> None:
        super().__init__()
        self.model = QStandardItemModel()

    def populate(self, path):
        self.path = path
        prefixes = get_scans(self.path)
        for scan in prefixes.keys():
            parent = QStandardItem("Group {}".format(scan))
            self.model.appendRow(parent)
            for i in prefixes[scan]:
                child = fileItem()
                child.setText("Scan {}".format(i))
                child.setCheckable(True)
                child.file = f"{self.path}/{scan}.{i}"
                parent.appendRow(child)

        self.setModel(self.model)


class Analysis_treeView(QW.QTreeView):
    def __init__(self) -> None:
        super().__init__()
        self.model = ScanModel()
        self.setModel(self.model)

    def populate(self, path):
        self.path = path
        prefixes = get_analysed_scans(self.path)

        for scan in prefixes.keys():
            parent = fileItem()
            parent.setText("Group {}".format(scan))
            self.model.appendRow(parent)
            for i in prefixes[scan]:
                child = analysis_item()
                child.setText("Scan {}".format(i))
                child.setCheckable(True)
                child.file = f"{self.path}/{scan}.{i}_analysed.h5"
                parent.appendRow([child, child.colorColumn])

    # def remove_item(slef,i):
