#!/usr/bin/env python
from PyQt5 import QtWidgets as QW
from PyQt5 import QtCore
import pyqtgraph as pg
from PyQt5.Qt import Qt
import sys  # We need sys so that we can pass argv to QApplication
import os
from viewopadata.utils import (
    load_data,
)
import numpy as np
from viewopadata.fits import fit_bessel, PolyFunctionFits
import h5py
from viewopadata.tree_utils import (
    ColorColumn,
    Analysis_treeView,
    Local_treeView,
)
from viewopadata.popup_2Ddata import PopUpWindow
import datetime


class MainWindow(QW.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        window_splitter = QW.QSplitter(Qt.Horizontal)
        plot_splitter = QW.QSplitter(Qt.Vertical)
        layout_plot = QW.QVBoxLayout()

        self.widget_plot = QW.QWidget()
        self.widget_plot_buttons = QW.QWidget()

        self.graphWidget = pg.PlotWidget()
        self.graphWidget.setBackground("w")
        # Add Title
        # Add Axis Labels
        pen1 = pg.mkPen("k", width=2)
        pen2 = pg.mkPen("r", width=1)
        self.raw_plot = self.graphWidget.plot([], [], pen=pen1)
        self.analyze_plot = self.graphWidget.plot([], [], pen=pen2)

        layout_plot.addWidget(self.graphWidget)

        self.range_input = QW.QWidget()
        self.gridLayout2 = QW.QGridLayout(self.range_input)
        self.name_range = QW.QLabel("Input analysis range")
        self.low_range = QW.QLineEdit("1200")
        self.low_range.textChanged.connect(self.onRangeChange)
        self.high_range = QW.QLineEdit("4500")
        self.high_range.textChanged.connect(self.onRangeChange)

        background_func_name = QW.QLabel("type of background")
        self.poly_bk_btn = QW.QRadioButton("Poly Bg")
        self.bessel_bk_btn = QW.QRadioButton("Bessel Bg")

        self.alanyze_btn = QW.QPushButton("Analyze")
        self.alanyze_btn.clicked.connect(lambda: self.fit_Bg(show=True))

        self.alanyze_andSave_btn = QW.QPushButton("Analyze and Save")
        self.alanyze_andSave_btn.clicked.connect(self.writeAnalysisFile)

        self.alanyze_all_andSave_btn = QW.QPushButton("Analyze ALL and Save")
        self.alanyze_all_andSave_btn.clicked.connect(self.writeAllAnalysisFile)

        layout_plot_buttons = QW.QGridLayout()

        layout_plot_buttons.addWidget(self.name_range, 0, 1)
        layout_plot_buttons.addWidget(self.low_range, 0, 2)
        layout_plot_buttons.addWidget(self.high_range, 0, 3)
        layout_plot_buttons.addWidget(background_func_name, 1, 1)
        layout_plot_buttons.addWidget(self.poly_bk_btn, 1, 2)
        layout_plot_buttons.addWidget(self.bessel_bk_btn, 1, 3)
        layout_plot_buttons.addWidget(self.alanyze_btn, 2, 1)
        layout_plot_buttons.addWidget(self.alanyze_andSave_btn, 2, 2)
        layout_plot_buttons.addWidget(self.alanyze_all_andSave_btn, 2, 3)

        self.widget_plot.setLayout(layout_plot)
        plot_splitter.addWidget(self.widget_plot)
        self.widget_plot_buttons.setLayout(layout_plot_buttons)
        plot_splitter.addWidget(self.widget_plot_buttons)

        window_splitter.addWidget(plot_splitter)
        # layout.addWidget(plot_splitter)

        # Add Background colour to white

        self.tree1_widget = QW.QWidget()
        tree1_layout = QW.QVBoxLayout()
        self.tree1 = Local_treeView()
        self.tree1.clicked.connect(self.on_tree_clicked)

        self.loadButton1 = QW.QPushButton("Load raw data folder")
        self.loadButton1.clicked.connect(self.onLoadbutton1)
        self.remove1button = QW.QPushButton("Remove selected")
        self.remove1button.clicked.connect(self.on_remove_button1)

        tree1_layout.addWidget(self.tree1)
        tree1_layout.addWidget(self.loadButton1)
        tree1_layout.addWidget(self.remove1button)

        self.tree1_widget.setLayout(tree1_layout)

        window_splitter.addWidget(self.tree1_widget)

        self.tree2_widget = QW.QWidget()
        tree2_layout = QW.QVBoxLayout()

        self.tree2 = Analysis_treeView()
        self.tree2.clicked.connect(self.on_tree2_clicked)
        tree2_layout.addWidget(self.tree2)

        self.loadButton2 = QW.QPushButton("Load analysis folder")
        self.loadButton2.clicked.connect(self.onLoadbutton2)
        tree2_layout.addWidget(self.loadButton2)

        self.plot2Dbutton = QW.QPushButton("plot2D data")
        self.plot2Dbutton.clicked.connect(self.onPlot2Ddata)
        tree2_layout.addWidget(self.plot2Dbutton)

        self.remove2button = QW.QPushButton("Remove selected")
        self.remove2button.clicked.connect(self.on_remove_button2)
        tree2_layout.addWidget(self.remove2button)

        self.tree2_widget.setLayout(tree2_layout)
        window_splitter.addWidget(self.tree2_widget)

        self.setCentralWidget(window_splitter)

    def on_tree_clicked(self, index):
        self.item = index.model().itemFromIndex(index)
        if hasattr(self.item, "file"):
            self.set_current_raw_data(self.item)
            self.plot_data_item()

    def on_tree2_clicked(self, index):
        self.raw_plot.setData([], [])

        self.item2 = index.model().itemFromIndex(index)
        if isinstance(self.item2, ColorColumn):
            self.item2.scan.randomColor()
            self.item2 = self.item2.scan
        if hasattr(self.item2, "plotDataItem"):

            if not self.item2.loaded:
                self.item2.loadData()
            if self.item2.checkState() == QtCore.Qt.Unchecked:

                if self.item2.isDisplayed:
                    self.graphWidget.removeItem(self.item2.plotDataItem)
                    self.item2.isDisplayed = False

            elif self.item2.checkState() == QtCore.Qt.Checked:
                if not self.item2.isDisplayed:
                    self.graphWidget.addItem(self.item2.plotDataItem)
                    self.graphWidget.setLabel(
                        axis="bottom", text="Energy", units="eV"
                    )

                    self.item2.isDisplayed = True

    def plot_data_item(self):
        self.analyze_plot.setData([], [])
        self.raw_plot.setData(self.item.X_raw, self.item.Y_raw)

    def plot_analysis_item(self):
        self.analyze_plot.setData([], [])
        self.raw_plot.setData(self.item2.X, self.item2.Y - self.item2.Y_fit)

    def plot_data_file(self, file=""):
        X, Y = load_data(file=file)
        self.local_x = X
        self.local_y = Y
        self.raw_plot.setData(self.local_x, self.local_y)
        self.onRangeChange()

    def set_current_raw_data(self, item):
        if not item.loaded:
            item.loadData()
        self.local_x = item.X_raw
        self.local_y = item.Y_raw
        self.onRangeChange()

    def onRangeChange(self):

        try:
            low = float(self.low_range.text())
            high = float(self.high_range.text())

            if (
                low >= self.local_x.min()
                and low < high
                and high < self.local_x.max()
            ):
                indx = np.logical_and(
                    low <= self.local_x, high >= self.local_x
                )

                self.analysis_x = self.local_x[indx]
                self.analysis_y = self.local_y[indx]

        except Exception as e:
            print("not good range", e)

    def fit_Bg(self, show=False):
        if self.poly_bk_btn.isChecked():

            results = PolyFunctionFits(
                npoly=6, ydata=self.analysis_y, xdata=self.analysis_x
            )
            self.fit_y = results[0]
        else:
            results = fit_bessel(self.analysis_x, self.analysis_y)
            self.fit_y = results[0].best_fit
        if show:
            self.analyze_plot.setData(self.analysis_x, self.fit_y)

    def onPlot2Ddata(self):
        parent = self.item2.parent()
        prefixes = []
        for i in range(parent.rowCount()):
            prefixes.append(parent.child(i).file)

        # file_prefix ='/Users/mihpop/Git_repos/hippie/opa_data_mihai_test/Analysis_results/2022-11-24_test_hippie.0_analysed.h5'

        # window = PopUpWindow(file_prefix=file_prefix)
        # print(prefixes)
        window = PopUpWindow(file_prefix=prefixes)

        if window.bool_good_data:
            window.exec()
        else:
            print("not good data")
            QW.QMessageBox.about(
                self,
                "ERROR",
                f"the Data in {parent.text()} contains multiple sizes",
            )

    def onLoadbutton1(self):
        folder_name = QW.QFileDialog.getExistingDirectory(
            self, "Select Directory"
        )
        self.raw_data_folder = folder_name
        self.tree1.populate(path=folder_name)

    def onLoadbutton2(self):
        folder_name = QW.QFileDialog.getExistingDirectory(
            self, "Select Directory"
        )
        self.tree2.populate(path=folder_name)

    def on_remove_button1(self):

        if hasattr(self.item, "file"):
            for i in range(self.tree1.model.rowCount()):
                a = self.tree1.model.item(i)

                nrows = a.rowCount()

                for j in range(nrows - 1, -1, -1):

                    data_item = a.child(j)
                    if data_item.checkState() == QtCore.Qt.Checked:
                        a.removeRow(j)
        else:
            nrows = self.item.rowCount()
            for j in range(nrows - 1, -1, -1):

                data_item = self.item.child(j)

                self.item.removeRow(j)

            idx = self.tree1.model.indexFromItem(self.item)
            self.tree1.model.removeRow(idx.row())
            self.item = None
        #     print(a.rowCount())

    def on_remove_button2(self):
        if hasattr(self.item2, "plotDataItem"):

            for i in range(self.tree2.model.rowCount()):
                a = self.tree2.model.item(i)

                nrows = a.rowCount()
                for j in range(nrows - 1, -1, -1):

                    data_item = a.child(j)

                    if data_item.checkState() == QtCore.Qt.Checked:

                        self.graphWidget.removeItem(data_item.plotDataItem)
                        data_item.isDisplayed = False
                        print(data_item.file)
                        a.removeRow(j)
        else:
            nrows = self.item2.rowCount()
            for j in range(nrows - 1, -1, -1):

                data_item = self.item2.child(j)
                self.graphWidget.removeItem(data_item.plotDataItem)
                data_item.isDisplayed = False
                print(data_item.file)
                self.item2.removeRow(j)

            idx = self.tree2.model.indexFromItem(self.item2)
            self.tree2.model.removeRow(idx.row())
            self.item2 = None

            # else:
            #     print('not checked')

    def writeAnalysisFile(self, item=None, folder=""):
        self.fit_Bg(show=False)
        if item is None:
            item = self.item
        if folder:
            wr_file = f"{os.path.basename(item.file)}_analysed.h5"
            wr_file = os.path.join(folder, wr_file)
        else:
            wr_file = f"{self.item.file}_analysed.h5"
        #print(item.text(), self.local_x)
        with h5py.File(wr_file, "w") as f:
            f["raw_x"] = self.local_x
            f["raw_y"] = self.local_y
            f["cut_x"] = self.analysis_x
            f["cut_y"] = self.analysis_y
            f["fit_y"] = self.fit_y
            f["time"] = self.item.T

    def writeAllAnalysisFile(self):
        # folder_name = QW.QFileDialog.getExistingDirectory(
        #     self, "Select Folder to save"
        # )
        loc_folder = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        loc_folder = "Analysis_" + loc_folder
        save_dir = os.path.join(self.raw_data_folder, loc_folder)
        os.mkdir(save_dir)
        if hasattr(self.item, "parent"):
            if self.item.parent() is None:
                print("Just ", self.item.text())
                for j in range(self.item.rowCount()):
                    try:
                        self.set_current_raw_data(self.item.child(j))
                        self.writeAnalysisFile(
                            folder=save_dir, item=self.item.child(j)
                        )
                    except Exception as e:
                        print(e)
            else:
                for i in range(self.tree1.model.rowCount()):
                    a = self.tree1.model.item(i)
                    # print(i,a.__dir__())
                    for j in range(a.rowCount()):
                        try:
                            self.set_current_raw_data(a.child(j))
                            self.writeAnalysisFile(
                                folder=save_dir, item=a.child(j)
                            )
                        except Exception as e:
                            print(e)
        print("Done Saving !")
        self.tree2.populate(path=save_dir)


def main():
    app = QW.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
