# viewopadata


A Gui to load and analyze OPA data

The GUI is installed by running "pip install ." in the OPA_ANALYSIS_GUI folder.
After, you can run it from a terminal with the command "OPA-GUI".

This project uses a special library from spectrocat called brukeropusreader
https://github.com/spectrochempy/brukeropusreader/

Because the above library proved diffecult to install we have added the used files to our repository. Thus every file in brukeropusreader is from the spectrochempy repository.
We have added the spectrochempy License file as well.

