#!/usr/bin/env python

###############################################################################
#     viewopadata
#
#     Copyright (C) 2021  MAX IV Laboratory, Lund Sweden.
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see [http://www.gnu.org/licenses/].
###############################################################################

from setuptools import setup, find_packages


setup(
    name="opaGUI",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    description="A Gui to load and analyze OPA data ",
    author="mihai",
    author_email="mihai.pop@maxiv.lu.se",
    license="GPLv3",
    url="https://gitlab.com/MaxIV/opa_analysis_gui",
    entry_points={
        "console_scripts": [
            "OPA-GUI=viewopadata.viewopadata:main",
        ]
    },
    packages=find_packages(exclude=("tests", "tests.*")),
    python_requires=">=3.6",
    install_requires=["h5py","python-dateutil", "lmfit", "pyqtgraph","pyqt5"],
)
